<?php
  //TODO1 : set the cookie parameter
  $httponly = TRUE;
  $path = "/";
  $domain = "sundars1.myblog.com";
  $secure = TRUE;
  session_set_cookie_params(60*15, $path, $domain, $secure, $httponly);
  session_start();
  require('../classes/db.php'); 
  require('../classes/user.php'); 

  if (isset($_POST["user"]) and isset($_POST["password"]) )
    if (User::login($_POST["user"],$_POST["password"])){
      $_SESSION["admin"] = User::SITE;
      $_SESSION["user"] = $_POST["user"];
      //TODO2 set the browser info to the session
      $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
    }  
      

  if (!isset($_SESSION["admin"] ) or $_SESSION["admin"] != User::SITE) {
    header( 'Location: /admin/login.php' ) ;
    die();
  }
  //TODO3 check the browser info in the session
  if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
	echo "Session hijacking is detected!";
	die();
	}
  
?>
