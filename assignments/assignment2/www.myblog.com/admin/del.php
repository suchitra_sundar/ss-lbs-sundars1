<?php 
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");
?>

<?php  
 //$post = Post::delete((int)($_GET["id"]));
  $post = Post::find($_GET['id']);
  if (isset($_POST['title'])) {
      $nocsrftoken = $_POST["nocsrftoken"];
      if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"]))
      {
        echo "CSRF Attack is detected!";
        die();
      }
  }
  header("Location: /admin/index.php");
?>

