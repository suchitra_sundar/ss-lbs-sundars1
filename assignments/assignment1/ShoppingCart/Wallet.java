import java.io.File;
import java.lang.String;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Wallet {
  
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
  this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws Exception {
        int currBalance=0;
      try{
  this.file.seek(0);

  currBalance = Integer.parseInt(this.file.readLine());
  while(currBalance == -1)
    this.file.seek(0);
}
catch (Exception e){
  System.out.println(e);
}

  return currBalance;

    }

    private void lock() throws Exception
    {
      this.file.setLength(0);
      this.file.writeBytes("-1");
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */

    /*public void setBalance(int newBalance) throws Exception {
  this.file.setLength(0);
  String str = new Integer(newBalance).toString()+'\n'; 
  this.file.writeBytes(str); 
    }*/

    public int safeWithdraw(int valueToWithdraw) throws Exception {
      int currBalance = this.getBalance();
      this.lock();
      if(currBalance < valueToWithdraw)
      {
        valueToWithdraw = currBalance;
        currBalance = 0;
      }
      else
      {
        currBalance -= valueToWithdraw;
      }
      this.file.setLength(0);
      this.file.writeBytes(String.valueOf(currBalance));
      return currBalance;
    }

    public void safeDeposit(int valueToDeposit) throws Exception {
      int currBalance = this.getBalance();
      this.lock();
      currBalance += valueToDeposit;
      this.file.setLength(0);
      this.file.writeBytes(String.valueOf(currBalance));
    }

   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
  this.file.close();
    }
}