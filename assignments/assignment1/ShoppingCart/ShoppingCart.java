import java.util.Scanner;
import java.util.Date;

public class ShoppingCart {
  public static void main(String[] args) throws Exception{
	Wallet wallet = new Wallet();
	int balance = wallet.getBalance();
	//Replace <YOUR NAME> below with your name!!! 
	System.out.println("Welcome to Suchitra Sundar's ShoppingCart. The time now is " + (new Date()).toString());
	System.out.println("Your balance: " + balance+ " credits\n");
	System.out.println("Please select your product: \n" + Store.asString());
	Scanner input = new Scanner(System.in);
	System.out.println("What do you want to buy, type e.g., pen ");
	String product = input.next();
	int price = Store.getPrice(product);
	if(balance>=price){
		int amount = wallet.safeWithdraw(price);
		{
			if(amount < price)
			{
				wallet.safeDeposit(amount);
				System.out.println("Your balance is less than the price");
				System.out.println("Your current balance: "+wallet.getBalance()+" credits");
			}
			else
			{
				Pocket pocket = new Pocket();
				pocket.addProduct(product);
				System.out.println("You new balance: "+ wallet.getBalance()+" credits");
			}
		}
	}else
	{
		System.out.println("Your balance is less than the price");
		System.out.println("Your current balance: "+wallet.getBalance()+" credits");

	}
  }
}