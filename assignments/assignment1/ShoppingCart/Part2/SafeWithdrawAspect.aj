import java.util.Date;
public aspect SafeWithdrawAspect{
	pointcut safeWithdraw(int price):call(* Wallet.safeWithdraw(int)) && args(price);
	before(int price):safeWithdraw(price) {
	try{
	Wallet obj = new Wallet();
	int balance = obj.getBalance();
	if(price <=balance){
	System.out.println("Amount withdrawn");
	}
	}catch(Exception ex){
	System.out.println("Exception");
	}
	}

	after(int price) returning (int withdrawnAmount):safeWithdraw(price){
	try{
	Wallet obj = new Wallet();
	if(withdrawnAmount<price){
	obj.safeDeposit(withdrawnAmount);
	System.out.println("Your balance is:" +withdrawnAmount);
	}
	}catch(Exception ex){
	System.out.println("Exception");
	}
	}
}
